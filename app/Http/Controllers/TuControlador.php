<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SistemaSolar;
use App\Models\Planeta;

// ...

class TuControlador extends Controller
{
    public function mostrarSistemasSolares()
{
    $sistemasSolares = SistemaSolar::all();
    return view('index_sistemas_solares', compact('sistemasSolares'));
}

public function mostrarFormularioSistemaSolar()
{
    return view('formulario_sistema_solar');
}

public function guardarSistemaSolar(Request $request)
{
    // Validar la entrada del formulario

    // Crear el sistema solar
    SistemaSolar::create([
        'nombre' => $request->input('nombre'),
        'edad_aproximada' => $request->input('edad_aproximada'),
        'tamano' => $request->input('tamano'),
        'descripcion' => $request->input('descripcion'),
    ]);

    return redirect()->route('mostrar_sistemas_solares')->with('status', 'Sistema Solar guardado exitosamente.');
}


// ...

public function editarSistemaSolar($id)
{
    $sistemaSolar = SistemaSolar::find($id);
    return view('formulario_editarsistema_solar', ['sistemaSolar' => $sistemaSolar]);
}

public function actualizarSistemaSolar(Request $request, $id)
{
    // Validar datos
    $request->validate([
        'nombre' => 'required',
        'edad_aproximada' => 'nullable|numeric',
        'tamano' => 'nullable|string',
        'descripcion' => 'nullable|string',
    ]);

    // Actualizar datos
    $sistemaSolar = SistemaSolar::find($id);
    $sistemaSolar->nombre = $request->nombre;
    $sistemaSolar->edad_aproximada = $request->edad_aproximada;
    $sistemaSolar->tamano = $request->tamano;
    $sistemaSolar->descripcion = $request->descripcion;
    $sistemaSolar->save();

    return redirect()->route('mostrar_sistemas_solares')->with('status', 'Sistema Solar actualizado correctamente');
}

public function eliminarSistemaSolar($id)
{
    // Eliminar el sistema solar con el ID proporcionado
    SistemaSolar::destroy($id);

    return redirect()->route('mostrar_sistemas_solares')->with('status', 'Sistema Solar eliminado correctamente');
}
// ...



public function mostrarPlanetas()
{
    $planetas = Planeta::with('sistemaSolar')->get();
    return view('index_planetas', compact('planetas'));
}

public function mostrarFormularioPlaneta()
{
    $sistemasSolares = SistemaSolar::all();
    return view('formulario_planeta', compact('sistemasSolares'));
}

public function guardarPlaneta(Request $request)
{
    // Validar la entrada del formulario

    // Crear el planeta asociado al sistema solar
    Planeta::create([
        'nombre' => $request->input('nombre'),
        'tipo' => $request->input('tipo'),
        'distancia_media' => $request->input('distancia_media'),
        'descripcion' => $request->input('descripcion'),
        'sistema_solar_id' => $request->input('sistema_solar_id'),
    ]);

    return redirect()->route('mostrar_planetas')->with('status', 'Planeta guardado exitosamente.');
}

public function editarPlaneta($id)
{
    $planeta = Planeta::findOrFail($id);
    $sistemasSolares = SistemaSolar::all();
    return view('formulario_editar_planeta', compact('planeta', 'sistemasSolares'));
}

public function actualizarPlaneta(Request $request, $id)
{
    // Validar la entrada del formulario

    // Encontrar el planeta por su ID
    $planeta = Planeta::findOrFail($id);

    // Actualizar los campos del planeta
    $planeta->update([
        'nombre' => $request->input('nombre'),
        'tipo' => $request->input('tipo'),
        'distancia_media' => $request->input('distancia_media'),
        'descripcion' => $request->input('descripcion'),
        'sistema_solar_id' => $request->input('sistema_solar_id'),
    ]);

    return redirect()->route('mostrar_planetas')->with('status', 'Planeta actualizado exitosamente.');
}

public function eliminarPlaneta($id)
{
    // Encontrar el planeta por su ID y eliminarlo
    Planeta::findOrFail($id)->delete();

    return redirect()->route('mostrar_planetas')->with('status', 'Planeta eliminado exitosamente.');
}

}

