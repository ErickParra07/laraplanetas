<?php
// app/Models/Planeta.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planeta extends Model
{
    protected $table = 'planetas'; // Agrega esta línea

    protected $fillable = ['nombre', 'sistema_solar_id', 'tipo', 'distancia_media', 'descripcion'];

    public function sistemaSolar()
    {
        return $this->belongsTo(SistemaSolar::class);
    }
};
