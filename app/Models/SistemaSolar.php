<?php
// app/Models/SistemaSolar.php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SistemaSolar extends Model
{
    protected $table = 'sistemas_solares'; // Agrega esta línea

    protected $fillable = ['nombre', 'edad_aproximada', 'tamano', 'descripcion'];

    public function planetas()
    {
        return $this->hasMany(Planeta::class);
    }
};

