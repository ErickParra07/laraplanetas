<?php

// Migración para la tabla 'planetas'
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanetasTable extends Migration
{
    public function up()
    {
        Schema::create('planetas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->unsignedBigInteger('sistema_solar_id');
            $table->enum('tipo', ['rocoso', 'gaseoso', 'helado'])->nullable();
            $table->float('distancia_media')->nullable();
            $table->text('descripcion')->nullable();
            // Agrega cualquier otro campo que necesites

            $table->foreign('sistema_solar_id')->references('id')->on('sistemas_solares');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('planetas');
    }
};
