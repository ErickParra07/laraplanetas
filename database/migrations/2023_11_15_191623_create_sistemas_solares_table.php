<?php
// Migración para la tabla 'sistemas_solares'
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSistemasSolaresTable extends Migration
{
    public function up()
    {
        Schema::create('sistemas_solares', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->integer('edad_aproximada')->nullable();
            $table->decimal('tamano', 8, 2)->nullable();
            $table->text('descripcion')->nullable();
            // Agrega cualquier otro campo que necesites
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sistemas_solares');
    }
};

