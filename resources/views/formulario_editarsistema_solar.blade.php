<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Sistema Solar</title>
    <style>
      body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #f9f9f9;
            margin: 0;
            padding: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column; /* Agregado para centrar verticalmente */
            height: 100vh;
        }

        h1 {
            margin-bottom: 20px;
            color: #333; /* Cambiado a un tono de gris más oscuro */
        }

        form {
            width: 80%;
            max-width: 400px;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        label {
            font-weight: bold;
            color: #333;
            display: block;
            margin-top: 10px;
        }

        input,
        select,
        textarea {
            width: 100%;
            padding: 8px;
            margin-top: 6px;
            margin-bottom: 12px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            border-radius: 4px;
            font-size: 14px;
        }

        button {
            background-color: #ffc107; /* Cambiado a amarillo */
            color: #fff;
            padding: 10px 16px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 14px;
            transition: background-color 0.3s ease;
        }

        button:hover {
            background-color: #ffab00; /* Tonificado amarillo */
        }

        a {
            margin-top: 10px;
            color: #dc3545; /* Cambiado a rojo */
            text-decoration: none;
        }
    </style>
</head>
<body>
    <h1>Editar Sistema Solar</h1>

    <!-- Formulario para editar un sistema solar -->
    <form method="POST" action="{{ route('actualizar_sistema_solar', ['id' => $sistemaSolar->id]) }}">
        @csrf

        <label for="nombre">Nombre del Sistema Solar:</label>
        <input type="text" id="nombre" name="nombre" value="{{ $sistemaSolar->nombre }}" required>

        <label for="edad_aproximada">Edad Aproximada del Sistema Solar:</label>
        <input type="number" id="edad_aproximada" name="edad_aproximada" value="{{ $sistemaSolar->edad_aproximada }}">

        <label for="tamano">Tamaño del Sistema Solar:</label>
        <input type="text" id="tamano" name="tamano" value="{{ $sistemaSolar->tamano }}">

        <label for="descripcion">Descripción del Sistema Solar:</label>
        <textarea id="descripcion" name="descripcion">{{ $sistemaSolar->descripcion }}</textarea>

        <button type="submit">Guardar Cambios</button>
    </form>

    <a href="{{ route('mostrar_sistemas_solares') }}">Volver al Listado de Sistemas Solares</a>
</body>
</html>
