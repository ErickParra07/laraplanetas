<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar Planeta</title>
    <style>
        body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #f9f9f9;
            margin: 0;
            padding: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column; /* Agregado para centrar verticalmente */
            height: 100vh;
        }

        h1 {
            margin-bottom: 20px;
            color: #333; /* Cambiado a un tono de gris más oscuro */
        }

        form {
            width: 80%;
            max-width: 400px;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        label {
            font-weight: bold;
            color: #333;
            display: block;
            margin-top: 10px;
        }

        input,
        select,
        textarea {
            width: 100%;
            padding: 8px;
            margin-top: 6px;
            margin-bottom: 12px;
            box-sizing: border-box;
            border: 1px solid #ccc;
            border-radius: 4px;
            font-size: 14px;
        }

        button {
            background-color: #ffc107; /* Cambiado a amarillo */
            color: #fff;
            padding: 10px 16px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 14px;
            transition: background-color 0.3s ease;
        }

        button:hover {
            background-color: #ffab00; /* Tonificado amarillo */
        }

        a {
            margin-top: 10px;
            color: #dc3545; /* Cambiado a rojo */
            text-decoration: none;
        }
    </style>
</head>
<body>
    <h1>Editar Planeta</h1>

    <!-- Formulario para editar un planeta existente -->
    <form method="POST" action="{{ route('actualizar_planeta', ['id' => $planeta->id]) }}">
        @csrf

        <label for="nombre">Nombre del Planeta:</label>
        <input type="text" id="nombre" name="nombre" value="{{ $planeta->nombre }}" required>

        <label for="tipo">Tipo de Planeta:</label>
        <select id="tipo" name="tipo">
            <option value="rocoso" @if($planeta->tipo == 'rocoso') selected @endif>Rocoso</option>
            <option value="gaseoso" @if($planeta->tipo == 'gaseoso') selected @endif>Gaseoso</option>
            <option value="helado" @if($planeta->tipo == 'helado') selected @endif>Helado</option>
        </select>

        <label for="distancia_media">Distancia Media del Planeta:</label>
        <input type="text" id="distancia_media" name="distancia_media" value="{{ $planeta->distancia_media }}">

        <label for="descripcion">Descripción del Planeta:</label>
        <textarea id="descripcion" name="descripcion">{{ $planeta->descripcion }}</textarea>

        <label for="sistema_solar_id">Sistema Solar:</label>
        <select id="sistema_solar_id" name="sistema_solar_id">
            @foreach ($sistemasSolares as $sistemaSolar)
                <option value="{{ $sistemaSolar->id }}" @if($planeta->sistema_solar_id == $sistemaSolar->id) selected @endif>{{ $sistemaSolar->nombre }}</option>
            @endforeach
        </select>

        <button type="submit">Actualizar</button>
    </form>

    <a href="{{ route('mostrar_planetas') }}">Volver al Listado de Planetas</a>
</body>
</html>

