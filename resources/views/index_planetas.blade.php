<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de Planetas</title>
    <style>
        body {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            background-color: #f9f9f9;
            margin: 0;
            padding: 0;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column; /* Agregado para centrar verticalmente */
            height: 100vh;
        }

        h1 {
            margin-bottom: 20px;
            color: #333; /* Cambiado a un tono de gris más oscuro */
        }

        table {
            width: 80%;
            max-width: 800px;
            border-collapse: collapse;
            margin-top: 20px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #333; /* Cambiado a un tono de gris más oscuro */
            color: #fff;
        }

        .actions {
            width: 120px;
            text-align: center;
        }

        .edit {
            background-color: #ffc107; /* Cambiado a amarillo */
            color: #fff;
            padding: 5px;
            border-radius: 4px;
            text-decoration: none;
            margin-right: 5px;
            margin-bottom: 5px; /* Añadido margen inferior */
        }

        .delete {
            background-color: #dc3545; /* Cambiado a rojo */
            color: #fff;
            padding: 5px;
            border-radius: 4px;
            text-decoration: none;
        }

        a {
            margin-top: 20px;
            color: #007bff;
            text-decoration: none;
        }

        .black {
            background-color:#000000; /* Cambiado a amarillo */
            color: #fff;
            padding: 5px;
            border-radius: 4px;
            text-decoration: none;
            margin-right: 5px;
            margin-bottom: 5px; /* Añadido margen inferior */
        }
    </style>
</head>
<body>
<a class="black" href="{{ route('mostrar_formulario_planeta') }}">Crear nuevo Planeta</a>
    <h1>Listado de Planetas</h1>

    @if(session('status'))
        <p>{{ session('status') }}</p>
    @endif

    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Tipo</th>
                <th>Distancia Media</th>
                <th>Descripción</th>
                <th>Sistema Solar</th>
                <th class="actions">Acciones</th> <!-- Nueva columna -->
            </tr>
        </thead>
        <tbody>
            @foreach ($planetas as $planeta)
                <tr>
                    <td>{{ $planeta->id }}</td>
                    <td>{{ $planeta->nombre }}</td>
                    <td>{{ $planeta->tipo }}</td>
                    <td>{{ $planeta->distancia_media }}</td>
                    <td>{{ $planeta->descripcion }}</td>
                    <td>{{ $planeta->sistemaSolar->nombre }}</td>
                    <td class="actions">
                        <a class="edit" href="{{ route('editar_planeta', ['id' => $planeta->id]) }}">Editar</a>
                        <br>
                        <br>
                        <a class="delete" href="{{ route('eliminar_planeta', ['id' => $planeta->id]) }}">Eliminar</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

 
</body>
</html>
