<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TuControlador;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/sistemas-solares', [TuControlador::class, 'mostrarSistemasSolares'])->name('mostrar_sistemas_solares');
Route::get('/sistemas-solares/crear', [TuControlador::class, 'mostrarFormularioSistemaSolar'])->name('mostrar_formulario_sistema_solar');
Route::post('/sistemas-solares/crear', [TuControlador::class, 'guardarSistemaSolar'])->name('guardar_sistema_solar');
Route::get('/sistemas-solares/editar/{id}', [TuControlador::class, 'editarSistemaSolar'])->name('editar_sistema_solar');
Route::post('/sistemas-solares/actualizar/{id}', [TuControlador::class, 'actualizarSistemaSolar'])->name('actualizar_sistema_solar');
Route::get('/sistemas-solares/eliminar/{id}', [TuControlador::class, 'eliminarSistemaSolar'])->name('eliminar_sistema_solar');
//otras rutas
Route::get('/planetas', [TuControlador::class, 'mostrarPlanetas'])->name('mostrar_planetas');
Route::get('/planetas/crear', [TuControlador::class, 'mostrarFormularioPlaneta'])->name('mostrar_formulario_planeta');
Route::post('/planetas/crear', [TuControlador::class, 'guardarPlaneta'])->name('guardar_planeta');
Route::get('/planetas/editar/{id}', [TuControlador::class, 'editarPlaneta'])->name('editar_planeta');
Route::post('/planetas/actualizar/{id}', [TuControlador::class, 'actualizarPlaneta'])->name('actualizar_planeta');
Route::get('/planetas/eliminar/{id}', [TuControlador::class, 'eliminarPlaneta'])->name('eliminar_planeta');
Route::get('/', function () {
    return view('welcome');
});
